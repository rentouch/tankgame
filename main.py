from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.scatter import Scatter
from kivy.factory import Factory
from kivy.graphics import Line, Color
from kivy.clock import Clock
from kivy.core.window import Window

from kivy.core.audio import SoundLoader
from kivy.graphics import Rectangle
from kivy.uix.image import Image
from kivy.animation import Animation
from kivy.properties import NumericProperty, ReferenceListProperty, ObjectProperty, ListProperty
from kivy.vector import Vector

from kivy.graphics.transformation import Matrix
from kivy.utils import boundary
from math import radians
from math import atan2
from math import pi

import math
import random
import time


class GameArea(Widget):
    missiles = []
    tanks = []
    obstacles = []
    items = ListProperty()

    def __init__(self, **kwargs):
        super(GameArea, self).__init__(**kwargs)
        Clock.schedule_once(self.seed_obstacles)
        Clock.schedule_once(lambda x: self.seed_item(Factory.Live), 0)

        # Background texture
        texture = Image(source='images/seamless_dirt.jpg').texture
        texture.wrap = 'repeat'
        texture.uvsize = (8, 8)
        with self.canvas:
            Rectangle(size=(2048, 2048), texture=texture)

        tanks_y = Window.height/2
        self.add_tank(pos=(Window.width/4, tanks_y))
        self.add_tank(pos=((Window.width/4)*3, tanks_y))

    def on_items(self, *kwargs):
        if not self.items:
            self.seed_item(Factory.Live)

    def seed_obstacles(self, *kwargs):
        xes = random.sample(xrange(Window.width), Window.width/500)
        for x in xes:
            while True:
                y = random.randint(1, Window.height)
                obstacle = Factory.Obstacle()
                obstacle.pos = (x, y)
                for tank in self.tanks:
                    if tank.collide_widget(obstacle):
                        break
                else:
                    self.obstacles.append(obstacle)
                    self.add_widget(obstacle)
                    break

    def seed_item(self, widget):
        xes = random.sample(xrange(Window.width-70), Window.width/500)
        for x in xes:
            while True:
                y = random.randint(1, Window.height-70)
                item = widget()
                item.pos = (x, y)
                for tank in self.tanks:
                    if tank.collide_widget(item):
                        break
                for obstacle in self.obstacles:
                    if obstacle.collide_widget(item):
                        break
                else:
                    self.items.append(item)
                    self.add_widget(item)
                    break

    def update(self, *kwargs):
        for missile in self.missiles:
            missile.move()

    def add_missile(self, velocity, pos, owner):
        missile = Missile(velocity=velocity, owner=owner, pos=pos)
        self.add_widget(missile)
        self.missiles.append(missile)

    def add_tank(self, pos, init_hits=0):
        tank = Factory.Tank()
        tank.center = tank.start_pos = pos
        tank.hits = init_hits
        self.add_widget(tank)
        self.tanks.append(tank)


class Tank(Scatter):
    dragging = False
    start_pos = None
    last_shot_time = 0
    shot_count = 0

    def __init__(self, **kwargs):
        super(Tank, self).__init__(**kwargs)
        self.impact_sound = SoundLoader.load('sounds/impact.wav')
        with self.canvas.before:
            Color(0, 0, 1, 1)
            self.direction_ray = Line(points=(self.center_x, self.center_y, self.center_x, self.center_y), width=2)

    def on_touch_down(self, touch):
        if not self.collide_point(*touch.pos):
            return

        if self.dragging:
            print "dragging"
            return

        touch.grab(self)

        touch.apply_transform_2d(self.to_local)
        if self.box.collide_point(*touch.pos):
            self.dragging = True
        touch.apply_transform_2d(self.to_parent)

        return super(Tank, self).on_touch_down(touch)

    def on_touch_move(self, touch):
        if touch.grab_current is not self:
            return

        if not self.dragging:
            dx = touch.x - self.center_x
            dy = touch.y - self.center_y
            angle = boundary(atan2(dy, dx) * 360 / 2 / pi, -360, 360)

            angle_change = self.tower.rotation - angle

            rotation_matrix = Matrix().rotate(-radians(angle_change), 0, 0, 1)
            self.tower.apply_transform(rotation_matrix, post_multiply=True,
                                       anchor=self.tower.anchor)
            return True

        else:
            touch.apply_transform_2d(self.to_local)
            points = [self.direction_ray.points[0], self.direction_ray.points[1], touch.x, touch.y]
            self.direction_ray.points = points

        return super(Tank, self).on_touch_move(touch)

    def on_touch_up(self, touch):
        if touch.grab_current is not self:
            return

        if self.dragging:
            Clock.schedule_once(lambda x: self.move_to(touch.pos))

        # Shorten movement ray
        self.direction_ray.points = [self.direction_ray.points[0], self.direction_ray.points[1],
                                     self.direction_ray.points[0], self.direction_ray.points[1]]
        self.dragging = False

        return super(Tank, self).on_touch_up(touch)

    def move_to(self, pos):
        # Stop last animation
        Animation.cancel_all(self)
        # Get distance between current pos and end pos
        dist = math.sqrt((self.center_x - pos[0])**2 + (self.center_y - pos[1])**2)
        # Only move if distance is worthy to move
        if dist > 40:
            # Get angle of tank
            dx = pos[0]-self.center_x
            dy = pos[1]-self.center_y
            rad = math.atan2(dy, dx)
            alpha = math.degrees(rad)
            duration = dist/self.speed
            if alpha < 0:
                alpha += 360

            rot = alpha - self.box.rotation
            if rot < 180:
                rotation = rot
            else:
                rotation = rot - 360

            if rotation > 180:
                rotation = self.box.rotation - rotation
            else:
                rotation = self.box.rotation + rotation

            def ani_completed(*kwargs):
                ani2 = Animation(center=pos, duration=duration)
                ani2.bind(on_progress=self.moving)
                ani2.start(self)
            delta = abs(self.box.rotation - rotation)
            ani = Animation(rotation=rotation, duration=1.0 / 180.0 * delta)
            ani.bind(on_complete=ani_completed)
            ani.start(self.box)
        else:
            self.shoot()

    def moving(self, *kwargs):
        if not self.parent:  # we already died
            return
        self.do_collide()
        for item in self.parent.items:
            if self.box_collide_widget(item):
                self.parent.items.remove(item)
                self.parent.remove_widget(item)
                if item.type == "live":
                    self.lives += 1

    def box_collide_widget(self, widget):
        widget_x, widget_y = self.to_local(widget.x, widget.y)
        widget_right = widget_x + widget.width
        widget_top = widget_y + widget.height
        if self.box.right < widget_x:
            return False
        if self.box.x > widget_right:
            return False
        if self.box.top < widget_y:
            return False
        if self.box.y > widget_top:
            return False
        return True

    def do_collide(self):
        for obstacle in self.parent.obstacles:
            if self.box_collide_widget(obstacle):
                Animation.cancel_all(self)

                # Move tank away from obstacle
                obstacle_x, obstacle_y = self.to_local(obstacle.x, obstacle.y)
                obstacle_right = obstacle_x + obstacle.width
                obstacle_top = obstacle_y + obstacle.height
                if obstacle_x < self.box.x < obstacle_right:
                    self.x += 10
                if obstacle_x < self.box.right < obstacle_right:
                    self.x -= 10
                if obstacle_y < self.box.top < obstacle_top:
                    self.y -= 10
                if self.box.y < obstacle_y < self.box.top:
                    self.y += 10

    def shoot(self):
        # Make sure you can't shoot countless in a row. =>
        # You have to wait first 3 seconds.
        if self.shot_count == 0:
            self.last_shot_time = time.time()
        elif self.shot_count == 3:
            if self.last_shot_time+2 < time.time():
                self.shot_count = 0
                self.last_shot_time = time.time()
            else:
                return

        if not self.parent:
            return

        alpha = self.tower.rotation
        alpha = math.radians(alpha)
        vx = math.cos(alpha)
        vy = math.sin(alpha)
        center = self.to_parent(self.tower.center_x+(vx*64),
                                self.tower.center_y+(vy*64))
        self.parent.add_missile(velocity=(vx, vy), pos=center, owner=self)
        self.shot_count += 1

    def hit(self):
        if self.lives == 1:
            Animation.cancel_all(self)
            self.parent.tanks.remove(self)
            self.parent.add_tank(self.start_pos, init_hits=self.hits)
            self.parent.remove_widget(self)
        tr = 'in_out_bounce'
        ani = Animation(x=self.x+6, y=self.y+3, duration=0.05, t=tr) + \
              Animation(x=self.x-4, y=self.y-2, duration=0.02, t=tr) + \
              Animation(x=self.x, y=self.y, duration=0.02, t=tr)
        ani.start(self)
        self.lives -= 1
        self.impact_sound.play()


class Missile(Widget):
    velocity_x = NumericProperty(0)
    velocity_y = NumericProperty(0)
    velocity = ReferenceListProperty(velocity_x, velocity_y)
    owner = ObjectProperty()

    def move(self):
        # Update pos
        velo = self.velocity[0] * self.speed, self.velocity[1] * self.speed
        self.pos = Vector(*velo) + self.pos

        # Check if left the window
        if 0 > self.x or self.x > Window.width or 0 > self.y or self.y > Window.height:
            self.remove()
            return

        # Check if hit a target
        for tank in self.parent.tanks:
            if self.owner == tank:
                continue
            if self.do_collide(tank):
                self.owner.hits += 1
                tank.hit()
                self.remove()
                return

        # Check if hit a obstacle
        for obstacle in self.parent.obstacles:
            if self.collide_widget(obstacle):
                self.remove()

    def do_collide(self, tank):
        x, y = tank.to_local(self.x, self.y)
        return tank.box.collide_point(x, y)

    def remove(self):
        self.parent.missiles.remove(self)
        self.parent.remove_widget(self)


class GameApp(App):
    def build(self):
        game = GameArea()
        Clock.schedule_interval(game.update, 1.0 / 60.0)
        return game


if __name__ == '__main__':
    GameApp().run()